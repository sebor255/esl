from myhdl import *

#input is a 2 x 32 bit long
@block
def encipher(data_in, data_out, clk, reset):
    state = enum('CONCATENATE', 'ENCIPHER', 'STOP')
    stateEncipher = enum('SLICE', 'XOR', )
    counter = Signal(intbv(2)[8:])
    actual_state = Signal(state.CONCATENATE)
    actual_encip = Signal(stateEncipher.SLICE)
    plaintext1 = Signal(intbv(0)[32:])
    plaintext2 = Signal(intbv(0)[32:])
    converted = Signal(intbv(0)[64:])
    key = Signal(intbv(0)[64:])
    key1 = Signal(intbv(231)[8:0])
    key2 = Signal(intbv(32)[8:0])
    key3 = Signal(intbv(76)[8:0])
    key4 = Signal(intbv(200)[8:0])
    key5 = Signal(intbv(151)[8:0])
    key6 = Signal(intbv(8)[8:0])
    key7 = Signal(intbv(1)[8:0])
    key8 = Signal(intbv(79)[8:0])

    @always_seq(clk.posedge, reset=reset)
    def logic():


        if actual_state == state.CONCATENATE:       #takes 2 last blocks 32 bit long and concatenates them
            data_out.tvalid.next = 0
            data_out.tlast.next = 0
            data_in.tready.next = 1
            if data_in.tvalid == 1:
                if data_in.tlast != 1:
                    plaintext1.next = data_in.tdata
                elif data_in.tlast == 1:
                    plaintext2.next = data_in.tdata
                    actual_state.next = state.ENCIPHER

        elif actual_state == state.ENCIPHER:
            data_out.tvalid.next = 1
            data_out.tlast.next = 0
            data_in.tready.next = 0
            if data_in.tlast.next == 1:
                if actual_encip == stateEncipher.SLICE:                     #switch left to right and fulfill the key
                    converted.next = ConcatSignal(plaintext1, plaintext2)
                    key.next = ConcatSignal(key1, key2, key3, key4, key5, key6, key7, key8)
                    actual_encip.next = stateEncipher.XOR
                elif actual_encip == stateEncipher.XOR:
                    converted.next = converted ^ key
                    actual_encip.next = stateEncipher.SLICE
                    actual_state.next = state.STOP


        elif actual_state == state.STOP:
            data_in.tready.next = 0
            data_out.tvalid.next = 1
            if counter.next > 0:
                counter.next = counter - 1
                data_out.tlast.next = 0
                data_out.tdata.next = converted[32:]
            elif counter.next == 0:
                data_out.tlast.next = 1
                #if data_out.tready == 1:
                data_out.tdata.next = converted[64:32]
                actual_state.next = state.CONCATENATE
                counter.next = 2

        else:
            raise ValueError("Undefined state")

    return instances()
