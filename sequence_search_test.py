from myhdl import *
from random import randint
from sequence_search import sequence_search
from axis import Axis

def test_sequence_search():

    clk = Signal(bool(0))

    data_in = Axis(32)
    data_out = Axis(32)

    dff_inst = sequence_search(data_in, data_out, clk)

    @always(delay(10))
    def clkgen():
        clk.next = not clk

    @always(clk.negedge)
    def stimulus():
        data_in.next = randint(3, 6)

    return dff_inst, clkgen, stimulus


def simulate(timesteps):
    tb = traceSignals(test_sequence_search)
    sim = Simulation(tb)
    sim.run(timesteps)

simulate(10000)
